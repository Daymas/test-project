<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 19.05.2020
 * Time: 11:22
 */

namespace vendor\generation;


class Generation
{
    public static function manual()
    {
        $data = file_get_contents("views/manual",  FILE_USE_INCLUDE_PATH);
       // echo $data;
        require_once "views/manual.php";
    }
    public static function help($params = "")
    {
        $data = file_get_contents("views/help",  FILE_USE_INCLUDE_PATH);
        echo $data;
    }

    public static function color($text, $color = '')
    {
        $colors = [
            'header'    =>  "\033[95m",
            'gray'      =>  "\033[93m",
            'green'     =>  "\033[92m",
            'warning'   =>  "\033[93m",
            'fail'      =>  '\033[91m',
            'bold'      =>  "\033[1m",
            'underline' =>  "\033[4m"
        ];
        $endc = "\x1b[0m";

        if ($color){
            return $colors[$color]. $colors['bold']. $text . $endc;
        }
        return $text;
    }
}