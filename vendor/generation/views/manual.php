<?php

echo "My Framework " . self::color("1.0", 'green');

echo "\nUsage:
  command [options] [arguments]\n";

echo "Options:\n";
echo "  " . self::color("-h, help", "green") . "         Display this help message\n";
echo "  " . self::color("env", "green") . "              Display the current framework environment\n";
echo "  " . self::color("route:list", "green") . "       List all registered routes\n";

echo "\nCreated:\n";
echo "  " . self::color("controller", "green") . "       Create a new controller class\n";
echo "  " . self::color("model", "green") . "            Create a new models class\n";
echo "  " . self::color("widget", "green") . "           Create a new Widget class\n";