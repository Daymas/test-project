<?php


namespace vendor\database;

class DB
{
    /**
     * Реальный экземпляр одиночки почти всегда находится внутри статического
     * поля.
     */
    private static $_instance;
    protected $pdo;

    /* Считаем количестов SQL запросов */
    public static $countSQL = 0;
    public static $queries = [];

    /** Подключаем файл конфигурации базы данных */
//    private $configDB = require_once (ROOT . "/config/database.php");

    private static $DB_HOST = '';
	private static $DB_NAME = '';
	private static $DB_USER = '';
	private static $DB_PASS = '';
	private static $DB_SET_NAMES = '';

    /**
     * Конструктор Одиночки не должен быть публичным. Однако он не может быть
     * приватным, если мы хотим разрешить создание подклассов.
     */
    protected function __construct ()
    {
        $configDB = require_once (ROOT . "/config/database.php");
        self::$DB_HOST = $configDB['connections']['mysql']['host'];
        self::$DB_NAME = $configDB['connections']['mysql']['database'];
        self::$DB_USER = $configDB['connections']['mysql']['username'];
        self::$DB_PASS = $configDB['connections']['mysql']['password'];
        self::$DB_SET_NAMES = $configDB['connections']['mysql']['charset'];

        /* Опции для PDO, обработка ошибок, обработка массивов, кодировка БД*/
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '". self::$DB_SET_NAMES . "'"
        ];

        $this->pdo = new \PDO(
            'mysql:host=' . self::$DB_HOST . ';dbname=' .
            self::$DB_NAME,
            self::$DB_USER,
            self::$DB_PASS,
            $options
        );
    }

    /**
     * Одиночки не должны быть восстанавливаемыми из строк.
     */
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    /* Проверяем существование экземпляра класса DB
     * @return object
    */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    /* Делаем запрос к БД и в случае успеха будет возвращено true, иначе false
     * @params string $sql входящий запрос к БД
     * @return  bool
    */
    public function execute($sql, $params = [])
    {
        self::$countSQL++;
        self::$queries[] = $sql;
        $stmt = $this->pdo->prepare(trim($sql));
        return $stmt->execute($params);
    }

    /* Делаем запрос к БД и возвращаем результат в качестве массива
     * @params string $sql входящий запрос к БД
     * @return array
    */
    public function query($sql, $params = [])
    {
        self::$countSQL++;
        self::$queries[] = $sql;
        $stmt = $this->pdo->prepare(trim($sql));
        $result = $stmt->execute($params);
        if ($result !== false){
            return $stmt->fetchAll();
        }
        return [];
    }
}