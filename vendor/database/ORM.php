<?php


namespace vendor\database;


use http\Env\Request;
use MongoDB\Driver\Query;

abstract class ORM
{
    /* @example Выборка из базы данных
    *  @params string $table, array $field, string ($where, $order, $up)
    *  @return  array
    */
    protected function select_db($table, $fields, $where="", $order="", $up=true, $limit="")
    {
        $table = $table ?: $this->table;
        for ($i = 0; $i < count($fields); $i++) {
            if ((strpos($fields[$i], "(") === false && ($fields[$i] != "*"))) $fields[$i] = "`".$fields[$i]."`";
        }
        $fields = implode(",", $fields);
        //  $table_name = $this->config->db_prefix.$table_name;
        if (!$order) $order = "ORDER BY `id`";
        else
        {
            if ($order != "RAND"){
                $order = "ORDER BY `$order`";
                if (!$up) $order .= " DESC";
            }
            else $order = "ORDER BY $order";
        }
        if ($limit) $limit = "LIMIT $limit";
        if ($where) $query = "SELECT $fields FROM $table WHERE $where $order $limit";
        else $query = "SELECT $fields FROM $table $order $limit";
        $result_set = $this->pdo->query($query);
        if (!$result_set) return(false);
        return $result_set;
    }

    /* @example  Добавляем информацию в базу данных
    *  @params string $table, array $new_value
    *  @return  Request
    */
    protected function insert_db($table_name, $new_value)
    {
        $table_name = $table_name ?: $this->table;
        $query      = "INSERT INTO $table_name (";
        foreach ($new_value as $field => $value) $query .= "`".$field."`,";
        $query      = substr($query , 0, -1); // Убираем последнюю запятую
        $query     .= ") VALUES (";
        foreach ($new_value as $value) $query .= "'".addslashes($value)."',";
        $query      = substr($query , 0, -1); // Убираем последнюю запятую
        $query     .= ")";
        return $this->query($query);

    }

    /*  @example  Редактируем информацию в базе данных
     *  @params string $table, $upd_fields, $where
     *  @return  bool
     */
    private function update_db($table_name, $upd_fields, $where)
    {
        $table_name = $table_name ?: $this->table;
        $query      = "UPDATE $table_name SET ";
        foreach ($upd_fields as $field => $value) $query .= "`$field` = '".addslashes($value)."',";
        $query     = substr($query , 0, -1); // Убираем последнюю запятую
        if ($where)
        {
            $query .= "WHERY $where";
            return $this->query($query);
        }
        else return false;
    }

    /*  @example  Добавляем информацию в базу данных
     *  @params string $table, $upd_fields, $where
     *  @return  bool
     */
    protected function delete_db($table_name, $where="")
    {
        $table_name = $table_name ?: $this->table;
        if ($where){
            $query   = "DELETE FROM `$table_name` WHERE `$where`";
            return  $this->query($query);
        }
        else return false;
    }

    /*  @example  Удаляем все данные из таблицы
     *  @params string $table
     *  @return  Query
     */
    protected function deleteAll($table_name = '')
    {
        $table_name = $table_name ?: $this->table;
        $query      = "TRANCATE TABLE `$table_name`";
        return      $this->query($query);
    }

    /*  @example  Просмотр определенного поля таблицы
     *  @params string $table
     *  @return  array
     */
    protected function getField($table_name, $field_out, $field_in, $value_in)
    {
        $data = $this->select_db($table_name, array($field_out), "`$field_in` = '".addslashes($value_in)."'");
        if (count($data) != 1) return false;
        return $data[0][$field_out];
    }

    /* @example Выборка из базы данных все записи
    *  @params string $table, $order='', $up=''
    *  @return  array
    */
    protected function getAll($table = '', $order='', $up='')
    {
        $table = $table ?: $this->table;
        /** Сортировка массива по возрастанию и убыванию через функцию select_db() */
        return $this->select_db($table, array("*"), "", $order, $up);
    }

    /* @example Проверка существования записи в таблице
     *  @params string $table, $field, $value
     *  @return  bool
     */
    private function isExists($table_name, $field, $value)
    {
        $data = $this->select_db($table_name, array("{$this->pk}"), "`$field` = '".addslashes($value)."'");
        if (count($data) === 0) return(false);
        return(true);
    }

    /* @example Проверка существования ID записи в таблице
     *  @params string $table, $ID
     *  @return  bool
     */
    private function existsID($table_name, $id){
        $data = $this->select_db($table_name, array("{$this->pk}"), "`{$this->pk}` = '".addslashes($id)."'");
        if (count($data) === 0) return(false);
        else return(true);
    }


    /*  @example Удаление записи в таблице по полю ID
     *  @params string $table, sql ID
     *  @return  bool
     */
    protected function deleteOnID($table_name, $id="")
    {
        $id = $id ?: $this->pk;
        if (!$this->existsID($table_name, $id)) return(false); // Проверяем кол-во записей в таблице
        return $this->delete_db($table_name, "`id` = '$id'");
    }

    /*  @example Изменение определённого поля
     *  @params string $table, $field, $value, $field_in, $value_in
     *  @return  bool
     */
    protected function setField($table_name, $field, $value, $field_in, $value_in)
    {
        return $this->update_db($table_name, array($field => $value), "`$field_in` = '".addslashes($value_in)."'" );
    }

    /*  @example Изменение определённого поля по ID
     *  @params string $table, $field, $value, $field_in, $value_in
     *  @return  bool
     */
    protected function setFieldOnID($table_name, $id, $field, $value)
    {
        if (!$this->existsID($table_name, $id)) return(false); // Проверяем кол-во записей в таблице
        return $this->setField($table_name, $field, $value , "id", $id);
    }

    /*  @example Получение случайных записей в таблице
     *  @params string $table, int $count
     *  @return
     */
    public function getRandomElements($table_name, $count = 1)
    {
        return $this->select_db($table_name, array("*"), "RAND()", true, $count);
    }

    /*  @example Количество записей в таблице
     *  @params string $table
     *  @return Integer
     */
    public function getCount($table_name=''){
        $table_name = $table_name ?: $this->table;
        $data = $this->select_db($table_name, array("COUNT(`{$this->pk}`)"));
        return $data[0]["COUNT(`{$this->pk}`)"];
    }


}