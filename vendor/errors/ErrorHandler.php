<?php
/**
 * Класс обработки ошибок
 */

namespace vendor\errors;


class ErrorHandler
{
    private $config;
    public function __construct()
    {
        $this->config = require_once ROOT . "/config/app.php";
        if ($this->config['debug'])
        {
            error_reporting(-1);
        }
        else
        {
            error_reporting(0);
        }
        ob_start();
        set_error_handler([$this, 'errorHandler']);
        register_shutdown_function([$this, 'fatalErrorHandler']);

    }

    public function errorHandler($errno, $errstr, $errfile, $errline)
    {
        ob_end_clean();
        $this->displayError($errno, $errstr, $errfile, $errline);
        return true;
    }

    public function fatalErrorHandler()
    {
        $error = error_get_last();
        if (!empty($error) && $error['type'] & ( E_ERROR | E_PARSE | E_COMPILE_ERROR | E_CORE_ERROR))
        {
            ob_end_clean();
            $this->displayError($error['type'], $error['message'], $error['file'], $error['line']);
        }
        else{
            ob_end_flush();
        }
    }

    /* Вывод текущих ошибок на экран */
    protected function displayError($errno, $errstr, $errfile, $errline, $respose = 500)
    {
        http_response_code($respose);
        if ($this->config['debug'])
        {
            require_once "error_views/development.php";
        }
        else
        {
            require_once "error_views/production.php";
        }
        die();
    }
}