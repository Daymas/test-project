<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 01.04.2020
 * Time: 9:01
 */

namespace vendor\base;

use vendor\core\Registry;

abstract class BaseController
{
    /* Текущий маршрут и парметры (controller, action, params)
    * @var array
    */
    protected $route = [];

    /* Текущий вид
    * @var string
    */
    protected $view;

    /* Текущий шаблон (layout)
    * @var string
    */
    protected $layout;

    /* Пользовательские данные
    * @var array
    */
    protected $vars = [];

    /* Текущая дерриктория вида
    * @var string
    */
    protected $directory;

    /* Объект класса реестр
    * @var object
    */
    public $registry;

    public function __construct($route)
    {
        $this->route = $route;
        $this->view = $route[1];
        $this->registry = Registry::getInstance();
    }

    /* Создает объект вида. Передает настройки (шаблон, вид, дирректория вида, маршрут)
    * @params route array, layout string, view string, directory string
    * @return void
    */
    public function getView()
    {
        $vObj = new BaseView($this->route, $this->layout, $this->view, $this->directory);
        $vObj->render($this->vars);
    }

    /* Передает данные в вид
    * @params $vars - различные данные
    * @return void
    */
    public function set($vars)
    {
        $this->vars = $vars;
    }

    /* Проверка на Ajax запрос
    * @params $vars - Null
    * @return bool
    */
    public function is_Ajax()
    {
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            // Если к нам идёт Ajax запрос, то ловим его
           return true;
        }
        //Если это не ajax запрос
        return false;
    }

    /* Возвращаем ошибку 404
    * @params $vars - Null
    */
    public function getError()
    {
        http_response_code(404);
        include_once ROOT . "/views/404/404.php";
    }

    /* Подключение вида при обработке Ajax запросоа
     * @params string $view, array $vars
     *
    */
    public function loadView($view, $vars = [])
    {
        extract($vars);
        require ROOT . "/views/{$this->controllerName()}/{$view}.php";
    }

    /* Название каталога вида, извлекается из названия контроллера.
    */
    private function controllerName()
    {
        $name = $this->route['controller'];
        return mb_strtolower(stristr($name, 'Controller', true));
    }
}