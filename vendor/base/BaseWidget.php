<?php
/**
 * Базовый клас для создания виджетов
 */

namespace vendor\base;


abstract class BaseWidget
{
    /**
     * The configuration array.
     * @var array
     */
    protected $config = [];

    public function __construct(array $config = [])
    {
        foreach ($config as $key => $value) {
            $this->config[$key] = $value;
        }
    }

    public function loadView($view, $vars = [])
    {
        extract($vars);
        require ROOT . "/views/widgets/{$view}.php";
    }
}