<?php


namespace vendor\base;

use vendor\database\DB;
use vendor\database\ORM;

abstract class BaseModel extends ORM
{
    protected $pdo; //Соединение с базой данных
    protected $table; // Название таблицы в БД
    protected $pk = 'id'; //Первичный ключ

    public function __construct()
    {
        $this->pdo = DB::getInstance();
    }

    /* Выполнение SQL запроса
     * @params string $sql  входящий sql запрос
     * @return  bool
     */
    public function query($sql)
    {
        return $this->pdo->execute($sql);
    }

    /* Поиск всех записей в таблице
     * @return  array
    */
    public function findAll()
    {
        $select = "SELECT * FROM {$this->table}";
        return $this->pdo->query($select);
    }

    /* Поиск одной записи по ключу
    * @params pkey -$id, array $field
    * @return  array, Limit 1
    */
    public function findOne($id, $field = '')
    {
        $field = $field ?: $this->pk;
        $select = "SELECT * FROM {$this->table} WHERE $field = ? LIMIT 1";
        return $this->queryArray($this->pdo->query($select, [$id]));
    }

    private function queryArray($arr)
    {
        $arrOut = array();
        array_map(function($a) use(&$arrOut){
            $arrOut = array_merge($arrOut,$a);
        }, $arr);
        return $arrOut;
    }

    /* Выполнение произвольного SQL запроса
    * @params string SQL
    * @return  array
    */
    public function findBySql($sql, $params = [])
    {
        return $this->pdo->query($sql, $params);
    }

    /* Выполнение SQL запроса c параметром
   * @params string SQL
   * @return  array
   */
    public function findLike($str, $field, $table = '')
    {
        $table = $table ?: $this->table;
        $select = "SELECT * FROM $table WHERE $field LIKE ?";
        return $this->pdo->query($select, ['%'. $str . '%']);
    }


}