<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 01.04.2020
 * Time: 10:21
 */

namespace vendor\base;

class BaseView
{
    /* Текущий маршрут и парметры (controller, action, params)
    * @var array
    */
    protected $route = [];

    /* Текущий вид (action)
    * @var string
    */
    protected $view;

    /* Текущий шаблон (layout)
    * @var string
    */
    protected $layout;

    /* Текущая дерриктория вида
    * @var string
    */
    protected $directory;

    public function __construct($route, $layout = '', $view = '', $directory='')
    {
        $this->route = $route;
        if($layout === false){
            $this->layout = false;
        }
        else{
            $this->layout = $layout ?: LAYOUT;
        }
        $this->view = $view;
        $this->directory = $directory;
    }

    /*@Генерирует страницу для отображения в шаблоне
    * @array $vars
    */
    public function render($vars)
    {
        if (is_array($vars)) extract($vars);
        $meta = new \vendor\core\Meta();  /* Объект мета данных */
        if(!$this->directory) $this->directory = $this->route[0];
        $file_view = ROOT . "/views/{$this->smallUpperCase($this->directory)}/{$this->smallUpperCase($this->view)}.php";
        ob_start();
        if(is_file($file_view)){
            require $file_view;
        }
        else{
            echo "<h3>Не найден файл вида <b>{$file_view}</b></h3>";
        }
        $content = ob_get_clean();
        if ($this->layout !== false){
            $file_layout = ROOT . "/views/layouts/{$this->layout}.php";
            if (is_file($file_layout)){
                require $file_layout;
            }
            else{
                echo "<h3>Не найден файл Шаблона <b>{$file_layout}!</b></h3>";
            }
        }
    }

    /* Приводит всю строку к нижнему регистру
    * @params строка string
    * @return string
    */
    private function smallUpperCase($str){
        return mb_strtolower($str);
    }
}