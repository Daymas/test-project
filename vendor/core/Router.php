<?php
namespace vendor\core;
class Router
{
    protected static $routes = []; //Массив, таблица всех маршрутов
    protected static $route = []; //Текущий маршрут
    protected static $params = []; //Переданные параметры

    //Функция обработки, добавления маршрута
    public static function add($regexp, $route = [])
    {
        self::$routes[$regexp] = $route;
    }

    //Возвращает список всех маршрутов
    public static function getRoutes()
    {
        return self::$routes;
    }

    //Возвращает текущий маршрут
    public static function getRoute()
    {
        return self::$route;
    }

    /* Проверяем маршрут введенный пользователем
     * params string $URL входящий url адрес
     * return  bool
    */
    protected static function MatchRoute($url)
    {
        $url = rtrim($url, "/");
        foreach (self::$routes as $pattern => $route) {
            if (preg_match("#^$pattern$#i", $url, $matches)) {
                self::parseParametrs($url, $pattern);
                self::$route = self::parseRoute($route);
                return true;
            }
        }
        return false;
    }

    /* Cоздание объектов и вызов метода контроллера
     * params string $URL входящий url адрес
     * return  void
    */
    public static function dispatch($url)
    {
        $url = self::removeQueryString($url);
        if (self::MatchRoute($url)) {
            try {
                $controller = 'app\controllers\\' . self::$route['controller'];
                if (!class_exists($controller)){
                    http_response_code(500);
                    throw new Exception("Invalid! <b>$controller</b> class not found.");
                }
                else{
                    $cObj = new $controller(self::$route);
                    try{
                        $action = self::$route['action'];
                        if(!method_exists($cObj, $action)){
                            http_response_code(500);
                            throw new Exception("Invalid! Method <b>$action</b> for controller <b>$controller</b> not found.");
                        }
                        else{
                            self::actionClassParams($cObj, $action);
                            $cObj->getView();
                        }
                    }catch (Exception $ex) {
                        //Выводим сообщение об исключении.
                        echo $ex->getMessage();
                    }
                }
            }catch (Exception $ex) {
                //Выводим сообщение об исключении.
                echo $ex->getMessage();
            }
        }else{
            http_response_code(404);
            include_once "../views/404/404.php";
        }
    }

    //Функция рабивает маршрут на составные части, возвращает контроллер и екшн.
    protected static function parseRoute($route)
    {
        $route = explode("@", $route);
        $route["controller"] = $route[0] . "Controller";
        $route["action"] = $route[1] . "Action";
        return $route;
    }

    /** Получаем параметры из URL
     * params string $URL входящий url адрес
     * return $url - обработанный массив
     */
    protected static function parseParametrs($url, $pattern)
    {
        $uri = explode("/", $url);
        if (count($uri) != 1){
            foreach ($uri as $value) {
                if (strpos($pattern, $value) === false) {
                    array_push(self::$params, $value);
                }
            }
        }
    }

    /**
     * Вызывает метод класса контроллера и передает парметры вызванному методу
     * @params self::$params - параметры для метода
     * @types private
     * @return void
     */
    private static function actionClassParams($cObj, $action)
    {
        $params = self::$params;
        switch (count($params)){
            case 0 : $cObj -> $action();
            break;
            case 1: $cObj -> $action($params[0]);
            break;
            case 2: $cObj -> $action($params[0], $params[1]);
            break;
            case 3: $cObj -> $action($params[0], $params[1], $params[2]);
            break;
        }
    }

    /**
     * Метод отвечает за обработку явных и не явных GET параметров
     * @params string $URL входящий url адрес
     * @types protected
     * @return string
     */
    protected static function removeQueryString($url)
    {
        if($url){
            $params = explode('&', $url, 2);
            if(false === strpos($params[0], "="))
                return rtrim($params[0], "/");
            else
                return '';
        }
    }

}