<?php


namespace vendor\core;


class Registry
{
    public static $objects = []; //Массив объектов

    private static $_instance;

    private static $cache;

    protected function __construct()
    {
        self::$cache = require_once ROOT . "/config/cache.php";
        foreach (self::$cache['components'] as $name => $component)
        {
            self::$objects[$name] = new $component;
        }
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    public function __get($name)
    {
        if (self::$objects[$name]){
            return self::$objects[$name];
        }
    }

    public function __set($name, $object)
    {
        if (!isset(self::$objects[$name])){
            self::$objects[$name] = new $object;
        }
    }
}