<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 06.05.2020
 * Time: 12:08
 */

namespace vendor\core;


class Meta
{
    /* Title страницы */
    private static $title = '';

    /* Описание страницы */
    private static $description = '';

    /* Ключевые слова */
    private static $keywords = '';

    /* Cкрипты на сайте */
    private static $scripts = [];

    /*@ Получаем Title текущей страницы
    * @var string $title
    */
    public static function setTitle($title)
    {
        self::$title = $title;
    }

    /*@ Отображаем Title на текущей странице
    */
    public static function getTitle()
    {
        if (self::$title !== '') {
            echo self::$title;
        }
        else{
            echo getcwd();
        }
    }

    /*@ Получаем Описание текущей страницы
    * @var string $description
    */
    public static function setDescription($description)
    {
        self::$description = $description;
    }

    /*@ Отображаем Описание текущей страницы
    */
    public static function getDescription()
    {
        if (self::$description !== '') {
            echo self::$description;
        }
        else{
            echo "Главная страница сайта!";
        }
    }

    /*@ Получаем список ключевых слов страницы
    * @var string $keywords
    */
    public static function setKeywords($keywords)
    {
        self::$keywords = $keywords;
    }

    /*@ Отображаем ключевые слова текущей страницы
    */
    public static function getKeywords()
    {
        if (self::$keywords !== '') {
            echo self::$keywords;
        }
        else{
            echo "HTML, META, метатег, тег, поисковая система";
        }
    }

    public static function setScripts($vars = [])
    {
        //array_push(self::$scripts, $vars);
        self::$scripts = $vars;
    }

    public static function getScripts()
    {
        if (count(self::$scripts) != 0){
            foreach (self::$scripts as $script){
                $script = trim($script, "/");
                echo "<script src=\"{$script}\"></script>";
            }
        }
        echo "";
    }

    public static function home_url()
    {
        $url = trim($_SERVER['REQUEST_URI'], "/");
        $url = explode("/", $url);

        if ( isset($_SERVER['HTTPS']) ) {
            $ssl = $_SERVER['HTTPS'];
        }elseif ( isset($_SERVER['HTTP_FRONT_END_HTTPS']) ) {
            $ssl = $_SERVER['HTTP_FRONT_END_HTTPS'];
        }else {
            $ssl = "OFF";
        }
        $root = (stripos($ssl, "ON") !== FALSE) ? "https" : "http";

        $url = "{$root}://localhost/{$url[0]}";
        return $url;
    }
}