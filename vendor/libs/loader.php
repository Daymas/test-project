<?php

/**
 * Функция автозагрузки классов
 */

spl_autoload_register(function ($class){
    $env = LIBS . "/env.php";
    $libs_function = LIBS . "/functions.php";
    if (is_file($libs_function)){
        require_once $libs_function;
    }
    if (is_file($env)){
        require_once $env;
    }
    $file = ROOT . '/' . str_replace("\\", "/", $class) . '.php';
    if (is_file($file)){
        require_once $file;
    }
    /*require_once __DIR__ . '../composer/autoload_real.php';
    return ComposerAutoloaderInit29103520b28dd38e2024b25f4a34b9dd::getLoader();*/
});
