<?php

//Распечатка в удобном виде.
function debug($arr){
    echo '<pre>' . print_r($arr, true) . '</pre>';
}

//Главная страницы сайта
function home_url(){
   $meta = new \vendor\core\Meta();
   return $meta::home_url();
}

//Вызов класса виджета
function Widget($name, $value = [])
{
    $name = ucfirst($name);
    $app = 'app\widgets\\' . $name . 'Widget';
    if (!class_exists($app)){
        throw new Exception("Invalid! <b>$app</b> class not found.");
    }
    else{
        if (count($value) > 0)
        {
            $wObj = new $app($value);
            $wObj->run();
        }
        else{
            $wObj = new $app();
            $wObj->run();
        }
    }
}

//Функция цвета для печати в консоли
function color($text, $color = '')
{
    $colors = [
        'header'    =>  '\033[95m',
        'gray'      =>  '\033[93m',
        'green'     =>  '\033[92m',
        'warning'   =>  '\033[93m',
        'fail'      =>  '\033[91m',
        'bold'      =>  '\033[1m',
        'underline' =>  '\033[4m'
    ];
    $endc = '\033[0m';

    if ($color){
        echo $colors[$color]. $text . $endc;
    }
    else echo $text;
}