<?php
/*Данная функция проверяет данные файла .env, нужна для файлов конфигов
*
* @params $var1, $var2
* @return string, bool
*/
function env($var1 = '', $var2 = '')
{
    if ($var1 != ''){
        if (is_file(ROOT."/.env")){
            $str = file_get_contents(ROOT. "/.env");
        }
        else {
            echo "<h3>Файл .env не найден!</h3>";
            exit();
        }
        preg_match_all("#([^\s]+)=([^\s]+)?#s",$str,$out);
        $env = array_combine($out[1],$out[2]) ;

        foreach ($env as $key => $value){
            if ($var1 == $key)
                return $value;
        }
    }
    if ($var2 != ''){
        return $var2;
    }
    return false;
}