<?php

/** Константы config/defines.php */
require_once dirname(__DIR__) . "/config/defines.php";

/* Register The Auto Loader  */
require_once ROOT . '/vendor/autoload.php';

/* Registry class object */
new \vendor\core\App();

