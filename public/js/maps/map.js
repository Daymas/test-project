jQuery(function($){
    $.getJSON("maps", function(data){
            initMap(data);
        });
});

function initMap(data) {
    ymaps.ready(init);
    var myMap;
    var pointer = [];
    $.each(data, function(key, val) {
        pointer.push(parse(val));
    });

    function init() {

        var i;
        var place;

        var myMap = new ymaps.Map("maps", {

            center: [48.015877, 37.802850],
            zoom: 10

        });

        for(i = 0; i < pointer.length; ++i) {

            place = new ymaps.Placemark(pointer[i]);
            myMap.geoObjects.add(place);
        }
    }
}



function circumference(r) {
    return parseFloat(r);
}

/*ymaps.ready(init);
var myMap;

function init() {

    var i;
    var place;
    var pointer = [[47.958183, 37.805410],[47.348402,40.032567],[47.987415,37.79887],[47.99306,37.798798]];

    var myMap = new ymaps.Map("maps", {

        center: [48.015877, 37.802850],
        zoom: 10

    });

    for(i = 0; i < pointer.length; i++) {

        place = new ymaps.Placemark(pointer[i]);
        myMap.geoObjects.add(place);
    }

}*/

function parse(element) {
    var array = element.split(",");
    var mas = [];
    for(i = 0; i < array.length; ++i) {
        mas.push(circumference(array[i]));
    }
    return mas;
}

//https://www.pandoge.com/stati-i-sovety/kak-postavit-neskolko-metok-na-yandekskartu-cherez-api