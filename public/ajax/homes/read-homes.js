function showHomes(col = '')
{
    changePageTitle("Список домов");
    if (col === "")
    {
            $.getJSON("homes/show", function(data){
            var read = readHome(data);
            $("#app").html(read);
            $("#mtable").tablesorter({
                widgets: ["vertical-group","zebra"]
            });
        });
    }
    else
    {

    }
}

//Построение таблицы со списком
function readHome(data)
{
    var read_homes_html = `
        <form method="POST" id="convert_form" action="export">
        <!-- начало таблицы -->
            <table id="mtable" class="table tablesorter" style="width: 100%">
            <thead>
            <!-- создание заголовков таблицы -->
            <tr>
                <th>Улица</th>
                <th>Номер дома</th>
                <th>Количество квартир</th>
                <th>Количество жильцов</th>
            </tr>
            </thead>
            <tbody id="tbody">`;
    read_homes_html += getHomesTR(data);
    read_homes_html += `</tbody></table>
     <input type="hidden" name="file_content" id="file_content" />
     <button type="button" name="convert" id="convert" class="btn btn-primary">Convert</button>
     </form>
    `;

    return read_homes_html;
}

//Функция отрисовки строки и столбцов таблицы
function getHomesTR(data) {
    var read_homes_html = "";
    var color = "";
    var people_list_btn = "";
    $.each(data, function(key, val) {
        //Цветовая схема полей
        if (val.count_people > 10) color = "color-One";
        else if (val.count_people < 3) color = "color-Two";
        else color = "color-Three";
        //Кнопка отображения списка жильцов
        if (val.count_people > 0) {
            people_list_btn = `<button class='btn btn-info' data-id='` + val.id + `' id="people_list">
                                    <i class="fas fa-users"></i>
                               </button>`
        } else people_list_btn = "";
        // создание новой строки таблицы для каждой записи
        read_homes_html += `
            <tr class="`+ color +`">
            <td aria-label="Улица">` + val.title + `</td>
            <td aria-label="Номер дома">` + val.number_home + `</td>
            <td aria-label="Кол-во квартир">` + val.count_quarters + `</td>
            <td aria-label="Кол-во жильцов">` + val.count_people + `</td>
            <!-- кнопки 'действий' -->
            <td w-25-pct text-align-center>
                <!-- кнопка Отображения жильцов в доме -->
                ` + people_list_btn +`
            </td>
        </tr>`;
    });
    return read_homes_html;
}


$(document).on('click', '#convert', function(){
        var table_content = '<table>';
        $('#mtable tr').find('td:last-child').remove()
        table_content += $('#mtable').html()
        table_content += '</table>';
        //console.log(table_content)
        $('#file_content').val(table_content);
        $('#convert_form').submit();

    showHomes();

});