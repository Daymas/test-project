/* Создание таблицы отображения списка жильцов при нажатии кнопки*/
$(document).on('click', '#people_list', function(event){
    event.preventDefault();
    var id = $(this).attr("data-id");
    innerTable(id);
});

function innerTable(id) {
    $.ajax({
        url: "homes/people",
        type : "POST",
        dataType : "JSON",
        data : id,
        complete: function () {

        },
        success : function(data) {
            var read = showPeoplesTable (data);
            $("#peoples").html(read);
        },
        error: function(xhr, resp, text) {
            // вывести ошибку в консоль
            console.log(xhr, resp, text);
        }
    });
}

function showPeoplesTable(data) {
    var read_peoples_html = `
        <!-- начало таблицы -->
            <table id="mtable" class="table tablesorter" style="width: 100%">
            <thead>
            <!-- создание заголовков таблицы -->
            <tr>
                <th>ФИО</th>
                <th>Улица</th>
                <th>Номер дома</th>
                <th>Номер квартиры</th>
            </tr>
            </thead>
            <tbody id="tbody">`;
    /* Формирование строк и столбцов таблицы */
    read_peoples_html += getPeopleTR(data);
    // конец таблицы
    read_peoples_html+=`</tbody></table>`;
    return read_peoples_html;
}

function getPeopleTR(data) {
    var read_peoples_html = "";
    $.each(data, function(key, val) {
        // создание новой строки таблицы для каждой записи
        read_peoples_html+=`
            <tr>
            <td aria-label="ФИО жильца">` + val.name +` ` + val.firstname +` ` + val.lastname + `</td>
            <td aria-label="Улица">` + val.title + `</td>
            <td aria-label="Номер дома" style="text-align: center">` + val.number_home + `</td>
            <td aria-label="Номер квартоиры" style="text-align: center">` + val.number_quarters + `</td>
        </tr>`;
    });
    return read_peoples_html;
}