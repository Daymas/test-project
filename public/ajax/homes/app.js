jQuery(function($){
    // HTML приложения
    showHomes();
});

// изменение заголовка страницы
function changePageTitle(page_title){
    // измение заголовка страницы
    $('#page-title').text(page_title);
    // измение заголовка вкладки браузера
    document.title=page_title;
}