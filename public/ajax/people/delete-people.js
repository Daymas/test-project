$(document).on('click', '#delete', function(){
    var peopleVal = $(this).attr("data-id");
    var result = confirm("Удалить запись?");
    if (result)
    {
        deletePeople(peopleVal);
    }
});

function deletePeople(id) {
    $.ajax({
        url: "people/delete",
        type : "POST",
        data : id,
        complete: function () {
            showPeoples();
        },
        success : function(result) {

        },
        error: function(xhr, resp, text) {
            // вывести ошибку в консоль
            console.log(xhr, resp, text);
        }
    });
}