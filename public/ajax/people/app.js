jQuery(function($){
    // HTML приложения
    showPeoples();
});

// изменение заголовка страницы
function changePageTitle(page_title){
    // измение заголовка страницы
    $('#page-title').text(page_title);
    // измение заголовка вкладки браузера
    document.title=page_title;
}

/* Обработка нажатия кнопки назад */
$(document).on('click', '#back', function(){
    btn_add_visible();
    showPeoples();
});

//Кнопка добавить - скрываем
function btn_add_hide() {
    $("#people_add").addClass("display-none");
}

//Кнопка добавить - показываем
function btn_add_visible() {
    $("#people_add").removeClass("display-none");
}