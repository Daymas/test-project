// функция для показа списка всех жильцов
function showPeoples() {
    changePageTitle("Список жильцов");
    btn_add_visible();
    $.getJSON("people/show", function(data){
        var read = readPeople(data);
        $("#app").html(read);
        $("#mtable").tablesorter({
            widgets: ["vertical-group","zebra"]
        });
    });
}
/* Создание таблицы отображения списка жильцов */
function readPeople(data)
{
    var read_peoples_html = `
        <!-- начало таблицы -->
            <table id="mtable" class="table tablesorter" style="width: 100%">
            <thead>
            <!-- создание заголовков таблицы -->
            <tr>
                <th>ФИО</th>
                <th>Улица</th>
                <th>Номер дома</th>
                <th>Номер квартиры</th>
            </tr>
            </thead>
            <tbody id="tbody">`;
    /* Формирование строк и столбцов таблицы */
    read_peoples_html += getPeopleTR(data);
    // конец таблицы
    read_peoples_html+=`</tbody></table>`;
    return read_peoples_html;
}
/* Формирование строк и столбцов таблицы */
function getPeopleTR(data)
{
    var read_peoples_html = "";
    $.each(data, function(key, val) {
        // создание новой строки таблицы для каждой записи
        read_peoples_html+=`
            <tr class="www">
            <td aria-label="ФИО жильца">` + val.name +` ` + val.firstname +` ` + val.lastname + `</td>
            <td aria-label="Улица">` + val.title + `</td>
            <td aria-label="Номер дома">` + val.number_home + `</td>
            <td aria-label="Номер квартоиры">` + val.number_quarters + `</td>
            <!-- кнопки 'действий' -->
            <td w-25-pct text-align-center>
                <!-- кнопка удаления -->
                <button class='btn btn-danger' data-id='` + val.id + `' id="delete">
                    <i class="fas fa-trash-alt"></i>
                </button>
            </td>
        </tr>`;
    });
    return read_peoples_html;
}