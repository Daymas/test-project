$(document).on('click', '#people_add', function(){
    // Cкрываем кнопку добавить
    btn_add_hide()
    //Меняем Title
    changePageTitle("Добавление жильца");
    // Вызываем метод добавления жильца peoplADD
    peopleADD();
});

function peopleADD() {
    //Получаем список улиц и формируем select
    $.getJSON("people/show/streets", function(data){
        var streets = showStreets(data);
        var form = formAdd(streets);
        $("#app").html(form);
    });
}

// Отображаем селект со списком улиц___
function showStreets(data) {
    var streets_html = `<select name='street_id' class='select-css' id="street_id">`;
    streets_html += `<option id="option_erase"> --- </option>`;
    $.each(data, function(key, val){
        streets_html += `<option name="str_id" value ='` + val.id + `'>` + val.title + `</option>`;
    });
    streets_html+=`</select>`;
    return streets_html;
}

function formAdd(streets) {
    var form_html = `
        <!-- html форма «Добавление нового жильца» -->
        <form id='create-people' action='#' method='post' border='0'>
            <table class='table table-hover table-responsive table-bordered' id="table_add_people">
                <tr>
                    <td>Фамилия</td>
                    <td><input type='text' name='name' class='form-control' required /></td>
                </tr>
                <tr>
                    <td>Имя</td>
                    <td><input type='text' name='firstname' class='form-control' required /></td>
                </tr>
                <tr>
                    <td>Отчество</td>
                    <td><input type='text' name='lastname' class='form-control' required /></td>
                </tr>
            <!-- список выбора улиц -->
                <tr>
                    <td>Улица</td>
                    <td>` + streets + `</td>
                </tr>
            </table>
        </form>
    `;
    var back = `<a id="back" href="#" name="back" class="back">Вернутся назад</a>`;
    return form_html + back;
}

function arase_optione() {
    $("#option_erase").remove();
}

$(document).on('change', '#street_id', function (event) {
    event.preventDefault();
    var streetVal = $("#street_id option:selected").val();
    arase_optione();
    if($("select").is("#home_id")) $("#tr_number_home").remove();
    if($("select").is("#quartres_id")) $("#tr_quartres_home").remove()
    if($("tr").is("#tr_submit")) $("#tr_submit").remove()
    $.ajax({
        type: 'GET',
        url: 'people/show/homes',
        dataType:"JSON",
        complete: function () {
            $(".cssload-loader").css("display", "none");
        },
        data: streetVal,
        success: function (data) {
            var number_homes = showNumberHome(data);
            numberHomeADD(number_homes);
        },
        error: function (xhr, str) {
            alert('Возникла ошибка: ' + xhr.responseCode);
        }
    });
});

function showNumberHome(data) {
    if($("select").is("#home_id")) $("#table_add_people tr:last").remove()
    var homes_html = `<select name='home_id' class='select-css' id="home_id">`;
    homes_html += `<option id="option_erase"> --- </option>`;
    $.each(data, function(key, val){
        homes_html += `<option value ='` + val.id + `'>` + val.number_home + `</option>`;
    });
    homes_html+=`</select>`;
    return homes_html;
}

function numberHomeADD(number_home) {
    var html = `<tr id="tr_number_home">
        <td>Номер дома</td>
        <td>` + number_home + `</td>
        </tr>`;
    $("#table_add_people").append(html);
}

/* Если выбран номер дома, нужно отобразить квартиры кот. есть в этом доме */

$(document).on('change', '#home_id', function (event) {
    event.preventDefault();
    var homeVal = $("#home_id option:selected").val();
    if($("tr").is("#tr_submit")) $("#tr_submit").remove()
    arase_optione();
    $.ajax({
        type: 'GET',
        url: 'people/show/quartres',
        dataType:"JSON",
        complete: function () {
            $(".cssload-loader").css("display", "none");
        },
        data: homeVal,
        success: function (data) {
            var number_quartres = showQuartresHome(data);
            quartresHomeADD(number_quartres);
        },
        error: function (xhr, str) {
            alert('Возникла ошибка: ' + xhr.responseCode);
        }
    });
});
//Cписок квартир_______
function showQuartresHome(data) {
    if($("select").is("#quartres_id")) $("#table_add_people tr:last").remove()
    var quartres_html = `<select name='quartres_id' class='select-css' id="quartres_id">`;
    quartres_html += `<option id="option_erase"> --- </option>`;
    $.each(data, function(key, val){
        quartres_html += `<option value ='` + val.id + `'>` + val.number_quarters + `</option>`;
    });
    quartres_html+=`</select>`;
    return quartres_html;
}
//Добавление списка квартир в таблицу____
function quartresHomeADD(number_quartres) {
    var html = `<tr id="tr_quartres_home">
        <td>Номер квартиры</td>
        <td>` + number_quartres + `</td>
        </tr>`;
    $("#table_add_people").append(html);
}
/* ===== Создание кнопки отправки ===== */
$(document).on('change', '#quartres_id', function (event) {
    if($("tr").is("#tr_submit")) $("#tr_submit").remove()
    event.preventDefault();
    arase_optione();
    var button = `<tr id="tr_submit">
            <td colspan="2">
                <button type='submit' id="submit" class='btn btn-primary'>
                    Добавить жильца
                </button>
            </td>
        </tr>`;
    $("#table_add_people").append(button);
});

/* === Обрабатываем нажатие кнопки отправить форму === */
// будет работать, если создана форма отправки

$(document).on('click', '#submit', function(){
    var $data = {};
// переберём все элементы input, textarea и select формы с id="myForm "
    $('#create-people').find ('input, select').each(function() {
        // добавим новое свойство к объекту $data
        // имя свойства – значение атрибута name элемента
        // значение свойства – значение свойство value элемента
        $data[this.name] = $(this).val();
    });
    //console.log($data)
    $.ajax({
        url: "people/create",
        type : "POST",
        data : $data,
        complete: function () {
            showPeoples();
        },
        success : function(result) {
            // Новый жилец был создан, вернуться к списку жильцов
            btn_add_visible();

        },
        error: function(xhr, resp, text) {
            // вывести ошибку в консоль
            console.log(xhr, resp, text);
        }
    });
});
