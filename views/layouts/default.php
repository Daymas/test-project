<?php //phpinfo()?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="<?php $meta::getDescription();?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php $meta::getTitle();?></title>
</head>
<body>
    <h1>Default</h1>

    <h4><?=$content;?></h4>


    <script src="<?= home_url(). "/jquery/jquery-3.2.1.min.js"?>"></script>
    <?php $meta::getScripts();?>
</body>
</html>

