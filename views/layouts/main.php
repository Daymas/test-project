<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="<?php $meta::getDescription();?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--===== Files CSS =====-->
    <link rel="stylesheet" href="css/style.css">
    <link href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" rel="stylesheet">
    <link href="css/theme.default.min.css" rel="stylesheet">
    <!--===== Files JS ======-->
    <script src="jquery/jquery.min.js"></script>
    <script src="jquery/jquery.timer.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?apikey=bae78c97-935f-460f-b1a2-9c06e69459fd&lang=ru_RU" type="text/javascript">
    </script>

    <title><?php $meta::getTitle();?></title>
</head>
<body>
<?php require_once "header_menu.php"?>

<div class="content" id="content">
    <?=$content;?>
</div>

<?php $meta::getScripts();?>
</body>
</html>