<header>
    <div class="logo">
        <a href="#"><img src="images/logo.png" alt="Главная страница"></a>
    </div>
    <nav>
        <ul class="nav-menu">
            <li class="nav-item"><a href="people">Жильцы</a></li>
            <li class="nav-item"><a href="homes">Дома</a></li>
            <li class="nav-item"><a href="maps">Карта</a></li>
            <li class="nav-item"><a href="homes-export">Export</a></li>
        </ul>
    </nav>
</header>