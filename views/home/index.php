<?php
$meta::setDescription("Жители, дома, жильцы");
$meta::setTitle("Список домов");
?>
    <div class="headers">
        <div>
            <ul class="hr">
                <li><b class="one">&nbsp;</b> Более 10 </li>
                <li><b class="two">&nbsp;</b> Мение 3</li>
                <li><b class="thre">&nbsp;</b> Остальные</li>
            </ul>
        </div>
        <div class="timer">
            <span id="countdown">05:00:00</span>
            <form id="example2form">
                <input type='button' class="fas fa-play" value=' &#xf04b; ' onclick='Example2.Timer.toggle();' />
                <input type='button' class="fas fa-power-off" value=' &#xf011; ' onclick='Example2.resetCountdown();' />
                <input type='button' class="fas fa-sync-alt" value=' &#xf2f1; ' onclick='Example2.reload();' />
                <input type='text' name='startTime' value='300' style='width:30px;' />
            </form>
        </div>
    </div>


    <main class="container" id="container">
        <section class="left-container">
            <div class="item"><h3 id="page-title"></h3></div>
        </section>
        <section class="right-container">
            <div class="item"></div>
        </section>

    </main> <!-- Заголовок -->

    <div class="app" id="app"></div>

    <div class="people_list" id="peoples"></div>

    <!-- Подгрузка скриптов -->
<?php $meta::setScripts(
    [
        home_url() . "/ajax/homes/app.js",
        home_url() . "/ajax/homes/read-homes.js",
        home_url() . "/ajax/homes/read-people.js",
        home_url() . "/ajax/homes/timer.js",
        home_url() . "/jquery/tablesorter.min.js",
    ]
);?>
