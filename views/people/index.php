<?php
    $meta::setDescription("Жители, дома, жильцы");
    $meta::setTitle("Список жильцов");
?>
<!--<div class="timer">Timer</div>-->

<main class="container" id="container">
    <section class="left-container">
        <div class="item"><h3 id="page-title"></h3></div>
    </section>
    <section class="right-container">
        <div class="item"><a href="#" class="people_add_btn" id="people_add">Добавить</a></div>
    </section>

</main> <!-- Заголовок -->

<div class="app" id="app">

</div>

<!-- Подгрузка скриптов -->
<?php $meta::setScripts(
    [
        home_url() . "/ajax/people/app.js",
        home_url() . "/ajax/people/read-people.js",
        home_url() . "/ajax/people/create-people.js",
        home_url() . "/ajax/people/delete-people.js",
        home_url() . "/jquery/tablesorter.min.js",
    ]
);?>
