-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Сен 12 2020 г., 13:24
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test-project`
--

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `homes`
--

CREATE TABLE `homes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `street_id` int(11) NOT NULL,
  `number_home` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `homes`
--

INSERT INTO `homes` (`id`, `street_id`, `number_home`) VALUES
(1, 21, 19),
(2, 18, 21),
(3, 9, 4),
(4, 27, 16),
(5, 12, 5),
(6, 11, 19),
(7, 9, 14),
(8, 27, 4),
(9, 16, 28),
(10, 12, 27),
(11, 27, 1),
(12, 24, 4),
(13, 19, 28),
(14, 23, 11),
(15, 29, 11),
(16, 20, 9),
(17, 2, 24),
(18, 3, 22),
(19, 23, 16),
(20, 15, 2),
(21, 21, 18),
(22, 1, 30),
(23, 25, 5),
(24, 6, 16),
(25, 6, 6),
(26, 8, 20),
(27, 24, 28),
(28, 24, 23),
(29, 1, 1),
(30, 2, 17);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_07_29_095845_create_people_table', 1),
(4, '2020_07_29_100049_create_streets_table', 1),
(5, '2020_07_29_100107_create_homes_table', 1),
(6, '2020_07_29_100132_create_quarters_table', 1),
(7, '2020_07_29_100238_create_residence_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `people`
--

CREATE TABLE `people` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `people`
--

INSERT INTO `people` (`id`, `name`, `firstname`, `lastname`) VALUES
(1, 'Лукин', 'Виль', 'Фёдорович'),
(2, 'Кулагин', 'Игнат', 'Евгеньевич'),
(3, 'Осипов', 'Павел', 'Фёдорович'),
(4, 'Пономарёвa', 'Антонина', 'Фёдоровна'),
(5, 'Жуков', 'Данила', 'Максимович'),
(6, 'Белов', 'Эдуард', 'Алексеевич'),
(7, 'Голубевa', 'Алёна', 'Фёдоровна'),
(8, 'Копыловa', 'Ева', 'Андреевна'),
(9, 'Потаповa', 'Елизавета', 'Максимовна'),
(10, 'Кузьминa', 'Вероника', 'Евгеньевна'),
(11, 'Силинa', 'Василиса', 'Дмитриевна'),
(12, 'Сухановa', 'Оксана', 'Владимировна'),
(13, 'Одинцов', 'Марат', 'Борисович'),
(14, 'Савинa', 'Софья', 'Фёдоровна'),
(15, 'Смирновa', 'Капитолина', 'Львовна'),
(16, 'Щукин', 'Матвей', 'Борисович'),
(17, 'Дорофеевa', 'Изольда', 'Алексеевна'),
(18, 'Соловьёв', 'Болеслав', 'Максимович'),
(19, 'Фадеевa', 'Мальвина', 'Дмитриевна'),
(20, 'Суворовa', 'Екатерина', 'Ивановна'),
(21, 'Мартыновa', 'Наталья', 'Александровна'),
(22, 'Фроловa', 'Изабелла', 'Ивановна'),
(23, 'Голубевa', 'Евгения', 'Александровна'),
(24, 'Романовa', 'Изабелла', 'Сергеевна'),
(25, 'Мамонтовa', 'Галина', 'Борисовна'),
(26, 'Давыдов', 'Трофим', 'Сергеевич'),
(27, 'Калинин', 'Рафаил', 'Иванович'),
(28, 'Пестовa', 'Ольга', 'Дмитриевна'),
(29, 'Кузнецовa', 'Регина', 'Евгеньевна'),
(30, 'Тарасовa', 'Алёна', 'Андреевна');

-- --------------------------------------------------------

--
-- Структура таблицы `quarters`
--

CREATE TABLE `quarters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `home_id` int(11) NOT NULL,
  `number_quarters` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `quarters`
--

INSERT INTO `quarters` (`id`, `home_id`, `number_quarters`) VALUES
(1, 18, 1),
(2, 29, 21),
(3, 11, 2),
(4, 28, 24),
(5, 6, 11),
(6, 29, 9),
(7, 4, 22),
(8, 2, 17),
(9, 22, 8),
(10, 10, 2),
(11, 26, 3),
(12, 23, 2),
(13, 26, 11),
(14, 9, 22),
(15, 23, 12),
(16, 22, 23),
(17, 8, 23),
(18, 3, 28),
(19, 17, 29),
(20, 23, 4),
(21, 16, 25),
(22, 13, 23),
(23, 29, 28),
(24, 15, 4),
(25, 20, 23),
(26, 9, 5),
(27, 18, 25),
(28, 2, 29),
(29, 23, 15),
(30, 29, 24);

-- --------------------------------------------------------

--
-- Структура таблицы `residence`
--

CREATE TABLE `residence` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `people_id` int(11) NOT NULL,
  `quarters_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `residence`
--

INSERT INTO `residence` (`id`, `people_id`, `quarters_id`) VALUES
(1, 18, 20),
(2, 1, 7),
(3, 3, 11),
(4, 6, 11),
(5, 2, 6),
(6, 19, 19),
(7, 19, 18),
(8, 10, 12),
(9, 12, 20),
(10, 2, 5),
(11, 30, 27),
(12, 7, 7),
(13, 4, 25),
(14, 1, 2),
(15, 5, 18),
(16, 14, 7),
(17, 9, 7),
(18, 11, 28),
(19, 27, 11),
(20, 25, 12),
(21, 23, 21),
(22, 2, 10),
(23, 3, 10),
(24, 14, 13),
(25, 26, 18),
(26, 19, 29),
(27, 4, 29),
(28, 23, 11),
(29, 30, 10),
(30, 30, 27);

-- --------------------------------------------------------

--
-- Структура таблицы `streets`
--

CREATE TABLE `streets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` char(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `streets`
--

INSERT INTO `streets` (`id`, `title`) VALUES
(1, 'Артёма'),
(2, 'Кобозева'),
(3, 'Красноармейская'),
(4, 'Октябрьская'),
(5, 'Первомайская'),
(6, 'Университетская'),
(7, 'Горького'),
(8, 'Челюскинцев'),
(9, 'Флеровского'),
(10, '50-летия СССР'),
(11, 'Трамвайная'),
(12, 'Набережная'),
(13, 'Доменная'),
(14, 'Коваля'),
(15, 'Донецкая'),
(16, 'Заречная'),
(17, 'Кальмиусская'),
(18, 'Малая набережная'),
(19, 'Ватутина'),
(20, 'Бирюзова'),
(21, 'Петровского'),
(22, 'Щорса'),
(23, 'Левобережная'),
(24, 'Розы Люксембург'),
(25, 'Маяковского'),
(26, 'Ходаковского'),
(27, 'Горячкина'),
(28, '230 Стрелковой дивизии'),
(29, 'Южная'),
(30, 'Независимости');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `homes`
--
ALTER TABLE `homes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `quarters`
--
ALTER TABLE `quarters`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `residence`
--
ALTER TABLE `residence`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `streets`
--
ALTER TABLE `streets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `homes`
--
ALTER TABLE `homes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `people`
--
ALTER TABLE `people`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `quarters`
--
ALTER TABLE `quarters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `residence`
--
ALTER TABLE `residence`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `streets`
--
ALTER TABLE `streets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
