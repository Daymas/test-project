<?php
/* Defines to project */
define('WWW', __DIR__);
define('ROOT', dirname(__DIR__));
define('CORE', dirname(__DIR__)."/vendor/core");
define('DB', dirname(__DIR__)."/vendor/database");
define('LIBS', dirname(__DIR__)."/vendor/libs");
define('APP', dirname(__DIR__)."/app");
define('RESOURCE', dirname(__DIR__)."/resource");
define('LAYOUT', 'default');


