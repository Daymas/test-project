<?php


namespace app\models;


class Streets extends Model
{
    protected $table = 'streets';

    /* Получаем список всех улиц */
    public function getStreets()
    {
        $sql = "select * from streets";
        $result = $this->findBySql($sql);
        return $result;
    }

}