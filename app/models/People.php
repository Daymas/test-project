<?php


namespace app\models;


class People extends Model
{
    protected $table = 'people';
    /* Выбираем всех жильцов с адресами их проживания
    * return name, firstname, lastname, title, number_home, number_quarters
    */
    public function getPeople(){
        $sql = "select people.id as id, name, firstname, lastname, title, number_home, number_quarters from people
                join residence r on people.id = r.people_id
                join quarters q on r.quarters_id = q.id
                join homes h on q.home_id = h.id
                join streets s on h.street_id = s.id";
        $result = $this->findBySql($sql);
        return $result;
    }

    /* Извдеч ID последней записи */
    public function getLastID()
    {
        $sql = "SELECT max(id) as id FROM people";
        return $this->findBySql($sql);
    }

    public function deletePeople($id){
        $sql = "delete from people where id = {$id}";
        $this->findBySql($sql);
    }

    public function getPeopleByHomeID($id)
    {
        $sql = "select people.id as id, name, firstname, lastname, title, number_home, number_quarters from people
                join residence r on people.id = r.people_id
                join quarters q on r.quarters_id = q.id
                join homes h on q.home_id = h.id
                join streets s on h.street_id = s.id
                where h.id = {$id}";
        return $this->findBySql($sql);
    }
}