<?php


namespace app\models;

use vendor\base\BaseModel;

abstract class Model extends BaseModel
{
    /*  @example Функция добавления данных в базу
     *  @params VALUE
     *  @return
     */
    public function add($new_value)
    {
        return($this->insert_db($this->table, $new_value));
    }

    /*  @example Функция редактирования данных по ID
     *  @params VALUE
     *  @return
     */
    public function edit($upd_fields, $value, $id='')
    {
        $id = $id ?: $this->pk;
        return $this->setFieldOnID($this->table, $id, $upd_fields, $value);
    }

}