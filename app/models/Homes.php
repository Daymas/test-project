<?php


namespace app\models;


class Homes extends Model
{
    protected $table = "homes";

    /*Выбор списка домов по ID*/
    public function getHomesByStreetID($id)
    {
        $sql = "select homes.id, number_home from homes
        join streets s on homes.street_id = s.id
        WHERE s.id = {$id}";
        return $this->findBySql($sql);
    }

    /* = Получаем список всех домов, с количеством квартир и количеством жильцов в квартирах = */
    public function getHomes($count = "")
    {
        $sql = "select  distinct homes.id, title, number_home,
                count(distinct number_quarters) as count_quarters,
                count( distinct people_id) as count_people
                from homes
                join streets s on homes.street_id = s.id
                left join quarters q on homes.id = q.home_id
                left join residence r on q.id = r.quarters_id
                group by number_home";
        if ($count !== ""){
            $sql = $sql . " LIMIT {$count}";
        }

        return $this->findBySql($sql);
    }

    public function getAdress()
    {
        $sql = "select title, number_home from homes
                join streets s on homes.street_id = s.id";

        return $this->findBySql($sql);
    }

}