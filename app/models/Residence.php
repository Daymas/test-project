<?php


namespace app\models;


class Residence extends Model
{
    protected $table = "residence";

/* Получаем список всех полей для редактирования */
    public function getAllPepople($id)
    {
        $sql = "select p.id, name, firstname, lastname, title, number_home, number_quarters from residence
                join people p on residence.people_id = p.id
                join quarters q on residence.quarters_id = q.id
                join homes h on q.home_id = h.id
                join streets s on h.street_id = s.id
                where people_id = {$id}";
        return $this->findBySql($sql);
    }

    public function deletePeopleID($id)
    {
        $sql = "delete from residence where people_id = {$id}";
        $this->findBySql($sql);
    }
}