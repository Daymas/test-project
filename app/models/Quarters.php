<?php


namespace app\models;


class Quarters extends Model
{
    protected $table = 'quarters';

    /*Выбор всех квартир по номеру дома*/
    public function getQuartersHomeID($id)
    {
        $sql = "select quarters.id, number_quarters from quarters
        join homes h on quarters.home_id = h.id
        where home_id = {$id}";
        $result = $this->findBySql($sql);
        return $result;
    }
}