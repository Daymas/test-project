<?php


namespace app\controllers;


class HomeController extends Controller
{
    protected $layout = "main";
    public function indexAction()
    {

    }

    /*Выводим список всех домов*/
    public function showAction()
    {
        if ($this->is_Ajax()){
            $home = $this->homes->getHomes();
            die(json_encode($home));
        }
        else $this->getError();
    }

    /*Выводим список всех жильцов в конкретном доме*/
    public function peopleAction()
    {
        if ($this->is_Ajax()){
            $home_id = array_keys($_POST)[0];
            $people = $this->people->getPeopleByHomeID($home_id);
            die(json_encode($people));
        }
        else $this->getError();
    }
}