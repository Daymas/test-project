<?php


namespace app\controllers;


class MapsController extends Controller
{
    protected $layout = "main";
    public function indexAction()
    {
        if ($this->is_Ajax()){
            $adress = $this->homes->getAdress();
            $maps = [];
            foreach ($adress as $adr){
                array_push($maps, $this->geoLocationCoordinate($adr));
            }
            die(json_encode($maps));
        }

    }

}