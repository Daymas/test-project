<?php


namespace app\controllers;


class PeopleController extends Controller
{
    protected $layout = "main";
    public function indexAction()
    {

    }

    /* Получаем список жильцов */
    public function showAction()
    {
        if ($this->is_Ajax()){
            $people = $this->people->getPeople();
            die(json_encode($people));
        }
        else $this->getError();
    }

    /* Получаем список улиц */
    public function streetsAction()
    {
        if ($this->is_Ajax()){
            $streets = $this->streets->getStreets();
            die(json_encode($streets));
        }
        else $this->getError();
    }

    //Получаем список домов
    public function homesAction(){
        if ($this->is_Ajax()){
            $street_id = array_keys($_GET);
            $number_homes = $this->homes->getHomesByStreetID($street_id[1]);
            die(json_encode($number_homes));
        }
        else $this->getError();
    }

    /*Получаем список квартир в доме по номеру*/
    public function quartresAction(){
        if ($this->is_Ajax()){
            $home_id = array_keys($_GET);
            if (isset($home_id[1])) {
                $home_id = $home_id[1];
                $number_quarters = $this->quatres->getQuartersHomeID($home_id);
                die(json_encode($number_quarters));
            }
        }
        else $this->getError();
    }

    /* Добавление нового жильца в базу */
    /* Добавляем нового жильца в базу */
    public function createAction()
    {
        if ($this->is_Ajax()){
            $quartres_id = $_POST['quartres_id'];
            if (!empty($_POST['name'])){
                if (!empty($_POST['firstname'])){
                    if (!empty($_POST['lastname'])){
                        $people = [
                            'name' => $_POST['name'],
                            'firstname' => $_POST['firstname'],
                            'lastname' => $_POST['lastname'],
                        ];
                    } else exit();
                }
                else exit();
            } else exit();
            $this->people->add($people);
            $people_id = $this->people->getLastID();
            $people_id = $people_id[0]['id'];

            $arr = [
                'people_id' => $people_id,
                'quarters_id' => $quartres_id,
            ];
            $this->residence->add($arr);
            die();
        }
        else $this->getError();
    }

    /* Удаляем жильца из базы
        удаляем данныее из таблиц people и residence
    */
    public function deleteAction()
    {
        if ($this->is_Ajax()){
            $people_id = array_keys($_POST)[0];
            $this->people->deletePeople($people_id);
            $this->residence->deletePeopleID($people_id);
            die();
        }
        else $this->getError();
    }

}