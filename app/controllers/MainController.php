<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 31.03.2020
 * Time: 12:42
 */

namespace app\controllers;


class MainController extends Controller
{
    protected $layout = "main";
    public function indexAction()
    {
        $data = ['title' => 'test'];
        $this->set(compact('data'));
    }

    public function showAction()
    {
        if ($this->is_Ajax()){
            $data = ['title' => 'test'];
            $this->loadView('show', compact('data'));
            die();
        }
        else $this->getError();
    }
}