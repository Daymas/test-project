<?php
/**
 * Created by PhpStorm.
 * User: Aleksandr
 * Date: 01.04.2020
 * Time: 12:55
 */

namespace app\controllers;

use app\models\Homes;
use app\models\People;
use app\models\Quarters;
use app\models\Residence;
use app\models\Streets;
use vendor\base\BaseController;

class Controller extends BaseController
{
    protected $people;
    protected $streets;
    protected $homes;
    protected $quatres;
    protected $residence;
    public function __construct($route)
    {
        parent::__construct($route);
        $this->people = new People();
        $this->streets = new Streets();
        $this->homes = new Homes();
        $this->quatres = new Quarters();
        $this->residence = new Residence();
    }

    protected function geoLocationCoordinate($adress)
    {
        $adress = urlencode("Донецк, ". $adress['title'] . ", дом ". $adress['number_home']);
        $url = "https://geocode-maps.yandex.ru/1.x/?apikey=bae78c97-935f-460f-b1a2-9c06e69459fd&geocode={$adress}";
        $content = file_get_contents($url);
        preg_match("/<pos>(.*?)<\/pos>/", $content, $point);
        $coords = explode(" ", strip_tags($point[1]));
        $coordinate = (float)$coords[1]. ",". (float)$coords[0];

        return $coordinate;
    }
}