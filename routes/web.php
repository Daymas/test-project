<?php
use vendor\core\Router;

Router::add('', 'Main@index');

//PeopleController methods
Router::add('people', 'People@index');
Router::add('people/show', 'People@show'); //Получаем список жильцов (AJAX)
Router::add('people/show/streets', 'People@streets'); //Получаем список улиц (AJAX)
Router::add('people/show/homes', 'People@homes'); //Получаем список домов (AJAX)
Router::add('people/show/quartres', 'People@quartres'); //Получаем список квартир в доме (AJAX)
Router::add('people/create', 'People@create'); //Добавляем данные о жильце в базу (AJAX)
Router::add('people/delete', 'People@delete'); //Удаляем запись о жильце (AJAX)

//HomeController
Router::add('homes', 'Home@index');
Router::add('homes/show', 'Home@show'); // Список домов (AJAX)
Router::add('homes/people', 'Home@people'); // Список жильцов в конкретном доме (AJAX)

//MapsController
Router::add('maps', 'Maps@index');
//Export Exel
Router::add('homes-export', 'Export@homes');
Router::add('export', 'Export@homes');